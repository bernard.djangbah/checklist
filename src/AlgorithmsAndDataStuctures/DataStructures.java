package AlgorithmsAndDataStuctures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataStructures {

    public static void main(String[] args) {
        // Sum equals problem
        List<Integer> nums = List.of(1,2,3,4,5,6,7);
        int target = 8;

        System.out.println(sumEquals(nums, target));
    }

    /*
    * Here I have sumEquals problem.
    * Given a list of numbers and a target, I am to check if there is a pair of numbers that adds up to the target
    * and return true if that is the case else return false.
    * This problem can be solved in a variety of ways but in my solution I chose to work with hash maps.
    * Since access times of maps are O(1) at best case, its makes this solution one of the most efficient.
    * In the end I go through the list once making this algo and O(N) time complexity algo
    * In terms of space, I created a map that stored the elements in the list again once also making the space complexity O(N)
    * This algo also used a basic for-in control flow that uses the underlying iterator in the list implementation
    * If-else was also used
    * */

    public static boolean sumEquals(List<Integer> nums, int target){
        Map<Integer, Integer> map = new HashMap<>();

        for (var i : nums){
            if (map.containsKey(i)){
                return true;
            } else {
                map.put(target - 1, i);
            }
        }
        return false;
    }
}
