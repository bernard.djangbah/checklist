import java_foundations.generics.MyLList;
import java.util.*;

public class Main {
    public static void main(String[] args) {
//        sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9});

//        System.out.println(getPermutation("ABCD"));

        MyLList<Integer> llist = new MyLList<>();
        llist.add(1);
        llist.add(2);
        llist.add(3);
        System.out.println(llist);
        llist.remove();
        System.out.println(llist.getSize());
        System.out.println(llist);
    }

    public static int sumGroups(int[] arr) {
        List<Integer> res = new ArrayList<>();
        boolean even = isEven(arr[0]);
        int sum = arr[0];

        if (arr.length == 1){
            return 1;
        }

        if (arr.length == 2 && even != isEven(arr[1])){
            return arr.length;
        } else if (arr.length == 2 && even == isEven(arr[1])){
            return 1;
        }

        for (int i = 1; i < arr.length; i++){
            if (isEven(arr[i]) == even && i == arr.length - 1){
                sum += arr[i];
                res.add(sum);
            } else if (isEven(arr[i]) != even && i == arr.length - 1){
                res.add(sum);
                sum = arr[i];
                res.add(sum);
            }
            else if (isEven(arr[i]) == even){
                sum += arr[i];
            }
            else {
                res.add(sum);
                even = isEven(arr[i]);
                sum = arr[i];
            }
        }

        if (canBeRepeated(res)){
            System.out.println(res);
            int[] arr1 = res.stream().mapToInt(i->i).toArray();
            return sumGroups(arr1);
        } else {
            System.out.println(res);
            return res.size();
        }
    }

    public static boolean canBeRepeated(List<Integer> nums){
        boolean even = isEven(nums.get(0));

        for (int i = 1; i < nums.size(); i++){
            if (even == isEven(nums.get(i))){
                return true;
            } else {
                even = isEven(nums.get(i));
            }
        }
        return false;
    }

    public static boolean isEven(int num){
        return num % 2 == 0;
    }

      public static void printArrayList(ArrayList<String> arrL)
        {
            arrL.remove("");
            for (String s : arrL) System.out.print(s + " ");
        }
        public static ArrayList<String> getPermutation(String str){
            if (str.length() == 0) {
                ArrayList<String> empty = new ArrayList<>();
                empty.add("");
                return empty;
            }

            char ch = str.charAt(0); //A or B or C
            //AB
            String subStr = str.substring(1);

            ArrayList<String> prevResult = getPermutation(subStr);

            ArrayList<String> Res = new ArrayList<>();

            for (String val : prevResult) {
                for (int i = 0; i <= val.length(); i++) {
                    Res.add(val.substring(0, i) + ch + val.substring(i));
                }
            }
            return Res;
        }
}