package OOP.dependencyInversion;

public class Chipper implements Payment {
    public void makePayment(double priceInCedis){
        System.out.println("Payment from store to Chipper with price "  + priceInCedis + " cedis");
    }
}
