package OOP.dependencyInversion;

public class ChipperPaymentService implements PaymentService {
    private final Payment payment = new Chipper();

    public void makePayment(double price, int quantity){
        payment.makePayment(price * quantity);
    }
}
