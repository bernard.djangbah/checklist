package OOP.dependencyInversion;

public class HubtelPaymentService implements PaymentService {
    private final Payment payment = new Hubtel();

    public void makePayment(double price, int quantity){
        payment.makePayment(price * 100 * quantity);
    }
}
