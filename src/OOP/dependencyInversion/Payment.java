package OOP.dependencyInversion;

public interface Payment {
    void makePayment(double price);
}
