package OOP.dependencyInversion;

public interface PaymentService {
    void makePayment(double price, int quantity);
}
