package OOP.dependencyInversion;

/*
this is a store class that is supposed to hold a quantity and price and allow someone to make payments with an external service,
chipper or hubtel. For simpliicity, it does not hold items.
 */

public class Store {
    private double price = 2.00;

    //Obviously this breaks the open-closed principe because our store is now tightly coupled with hubtel, what happens
    // when we want to pay with chipper instead? It means we will have to open store class and edit it fields and then change the
    // makePayment method to give the price in cedis instead of pesewas.

    // Bad solution
//    private Payment payment = new Hubtel();
//
//    public void makePayment(int quantity){
//        payment.makePayment(price * 100 * quantity);
//    }

    // now if we wanna use chipper we have to delete hubtel and replace it with chipper instance
    // getters and setters won't work since one takes cedis and other takes pesewas. We would have to chech the indtances of the
    // and write different payment solution for them this way which can produce ugly code

//    private Payment payment = new Chipper();
//    public void makePayment(int quantity){
//        payment.makePayment(price * quantity);
//    }

    // to solve this issue we can use the facade/adaptor method to create an intermediate payment service we can make payments to
    // and that intermediate service will decide where it wants to send the payment
    // this way, we don't have to edit our store class to change where we want to send payments
    private PaymentService paymentService;

    public Store (PaymentService paymentService){
        this.paymentService = paymentService;
    }

    public void makePayment(int quantity){
        paymentService.makePayment(price, quantity);
    }

    // if we want to use a different payment service we can just call this setter, set the new payment service and we're
    // good to go. We don't have to change our code. Obviously this code can be refactored better using a design pattern by
    // for simplicity this is fine
    public void setPaymentService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }
}
