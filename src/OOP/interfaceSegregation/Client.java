package OOP.interfaceSegregation;

public abstract class Client {
    protected String name;

    protected Client(String name) {
        this.name = name;
    }

    /*
    * this breaks the interface segregation principle since not all clients are going to pay insurance and take payments
    * at the same time
    * Obviously, HSPs(hospitals, pharmacies) are only going to take payments whiles individuals are going to pay insurance.
    * the solution to this is to divide these functionalities into 2 different interfaces and let the classes who does each
    * implement just that interface that has that particular method
    * */
//    public abstract void payInsurance();

//    public abstract void takePayment();
}
