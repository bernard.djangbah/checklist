package OOP.interfaceSegregation;

public class HSP extends Client implements PayTakers {

    protected HSP(String name) {
        super(name);
    }

    /*
    Because this extends client that has payInsurance, this class must still override it even though it does not have any
    implementation for it
    Instead, we could move the method into interfaces and only implement the interface that has only what we need
     */

//    @Override
//    void payInsurance() {
//    }

    @Override
    public void takePayment() {
        System.out.println("Taking payment for this month");
    }
}
