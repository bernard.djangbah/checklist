package OOP.interfaceSegregation;

public class Individuals extends Client implements Payers{
    protected Individuals(String name) {
        super(name);
    }

    @Override
    public void payInsurance() {
        System.out.println("I am paying my insurance");
    }
}
