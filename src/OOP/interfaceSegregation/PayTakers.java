package OOP.interfaceSegregation;

public interface PayTakers {
    void takePayment();
}
