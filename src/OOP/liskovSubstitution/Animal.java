package OOP.liskovSubstitution;

public abstract class Animal {
    protected String name;
    protected String color;

    protected Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
