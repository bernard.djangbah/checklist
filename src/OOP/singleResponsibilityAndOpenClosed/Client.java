package OOP.singleResponsibilityAndOpenClosed;

import java.sql.Timestamp;
import java.time.Instant;

/*
the SRP states that a class should only do one thing and only one thing at a time. It can be also applied to simple units
like methods and complex topics like microservices
 */

public class Client {
    private final String clientID;
    private final String name;

    public Client(String name){
        this.name = name;
        this.clientID = Timestamp.from(Instant.now()).toString();
    }

    public String getClientID() {
        return clientID;
    }

    public String getName() {
        return name;
    }
    public void makePayment(){
        System.out.println("Client with name " + name + " is making payment.");
    }

    /*
    Since this class is supposed to represent a client and behaviors associated with clients, adding sending email and sms
    to clients violates the SRP.
    If you think about it, the ability to send email to clients shouldn't really be on the client itself but on the entity
    sending the email.
    So it makes sense to extract the methods that sends message into different classes and let them accept a Client object
    that they perform the action on.
    As for make payment, it is strictly a client behaviour so that is fine.

    You can argue that if it is the client sending the email then this is fine, for this small example it can be. But when you
    introduce parents and subclasses (different types of clients) and they all have to send emails and messages, it would make sensea
    extract these methods into other classes too to allow for open and closed principle in the sense that if we want another
    way of sending messages, we could do that without modifying the clients themselves, but that is a different topic.
     */

    /*
    public void sendEmail(){
        System.out.println("Sending email to client with name " + name);
    }

    public void sendSMS(){
        System.out.println("Sending sms to client with name " + name);
    }
    */
}