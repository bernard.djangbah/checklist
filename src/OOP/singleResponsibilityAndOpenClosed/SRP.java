package OOP.singleResponsibilityAndOpenClosed;

public class SRP {
    public static void main(String[] args) {
        Client client = new Client("Turntable");
        Client client1 = new Client("Aisha");

        SendMessages service = new SendMessages();
        /*
        we can still send emails and sms to all the clients here even though the behavior is not on the clients themselves
         */
        service.sendEmail(client);
        service.sendSMS(client);
        service.sendEmail(client1);
        service.sendSMS(client1);
    }
}
