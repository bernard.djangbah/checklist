package OOP.singleResponsibilityAndOpenClosed;

/*
this is the extra we're extracting the send message methods into to enforce the SRP
in this way you can see that it also kinds of enforces open-closed principles in the sense that we can send messages
in different ways without changing the client themselves
this creates some loose coupling which makes code maintainable and testable
 */
public class SendMessages {
    public void sendEmail(Client client){
        System.out.println("Sending email to client with name " + client.getName());
    }

    public void sendSMS(Client client){
        System.out.println("Sending sms to client with name " + client.getName());
    }
}
