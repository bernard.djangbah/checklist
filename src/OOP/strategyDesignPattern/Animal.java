package OOP.strategyDesignPattern;

import java.util.ArrayList;
import java.util.List;

public abstract class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }

    protected void move(){

    }

    public String getName() {
        return name;
    }
}
