package java_foundations.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Collections {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(List.of(1, 2, 3, 4, 5, 6, 6, 6, 6));
        System.out.println(setToList(set));
    }

    public static <T> ArrayList<T> setToList(Set<T> set){
        return new ArrayList<>(set);
    }
}
