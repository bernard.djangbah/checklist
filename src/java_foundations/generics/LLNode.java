package java_foundations.generics;

public class LLNode<T> {
    private T value;
    private LLNode<T> next;

    public LLNode(T value){
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public LLNode<T> getNext() {
        return next;
    }

    public void setNext(LLNode<T> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
