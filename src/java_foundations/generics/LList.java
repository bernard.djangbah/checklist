package java_foundations.generics;

public interface LList<T>{
    void add(T element);
    T remove();
}
