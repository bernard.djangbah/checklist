package java_foundations.generics;

public class MyLList<T> implements LList<T>{
    private int size;
    private LLNode<T> head;

    public int getSize() {
        return size;
    }

    @Override
    public void add(T value) {
        LLNode<T> element = new LLNode<>(value);
        if (size != 0) {
            element.setNext(head);
        }
        head = element;
        size++;
    }

    @Override
    public T remove() {
        if (size == 0){
            return null;
        }
        LLNode<T> popped = head;
        head = head.getNext();
        popped.setNext(null);
        size--;
        return popped.getValue();
    }

    @Override
    public String toString() {
        LLNode<T> current = head;
        StringBuilder str = new StringBuilder("[");
        while (current.getNext() != null){
            str.append(current).append(", ");
            current = current.getNext();
        }
        str.append(current).append("]");
        return str.toString();
    }
}
