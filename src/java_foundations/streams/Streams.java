package java_foundations.streams;

import java.util.List;

public class Streams {
    public static void main(String[] args) {
        List<String> names = List.of("Bernard", "Tetteh", "Djangbah");
        System.out.println(reduceStringLengths(names));
    }

    public static int reduceStringLengths(List<String> list){
        return list.stream().map(String::length).reduce(0, Integer::sum);
    }
}